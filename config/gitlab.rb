# SSH port
gitlab_rails['gitlab_shell_ssh_port'] = 2222

external_url 'http://server:80'

letsencrypt['enable'] = false
nginx['listen_https'] = false

